﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pathfinding : MonoBehaviour
{
    public Transform[] point;
    private NavMeshAgent nav;
    private int deshPoint;

    private void Start()
    {
        nav = GetComponent<NavMeshAgent>();
    }

    private void FixedUpdate()
    {
        if (!nav.pathPending && nav.remainingDistance < 0.5f)
            NextPoint();
    }

    void NextPoint()
    {
        if (point.Length == 0)
            return;
        nav.destination = point[deshPoint].position;
        deshPoint += 1;
        deshPoint %= point.Length;
    }
}
